package br.edu.ifsc.atividade;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i("Ciclo de Vida", "OnCreate");
        Toast.makeText(getApplicationContext(), "onCreate", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onStart(){
        super.onStart();
        Log.i("Ciclo da Vida", "OnStart");
        Toast.makeText(getApplicationContext(),"onStart", Toast.LENGTH_LONG).show();
    }
    //Onresume onPause onStop onDestroy
    @Override
    protected void onResume(){
        super.onResume();
        Log.i("Ciclo de Vida", "onResume");
        Toast.makeText(getApplicationContext(),"onResume", Toast.LENGTH_LONG).show();
    }

    protected void onPause() {
        super.onPause();
        Log.i("Ciclo de Vida", "onPause");
        Toast.makeText(getApplicationContext(), "onPause", Toast.LENGTH_LONG).show();
    }
    protected void onStop() {
        super.onStop();
        Log.i("Ciclo de Vida", "onStop");
        Toast.makeText(getApplicationContext(), "onStop", Toast.LENGTH_LONG).show();
    }
    protected void onDestroy(){
        super.onDestroy();
        Log.i("Ciclo de Vida", "onDestroy");
        Toast.makeText(getApplicationContext(),"onDestroy", Toast.LENGTH_LONG).show();
}
